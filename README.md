
# Attendance Cheater - Python Bot

## Description:

This script is designed to automatically submit students' Hack Reactor attendance tokens. Every cohort at Hack Reactor has a non-stakes competition for submitting attendance tokens. This bot makes the process (almost) hands-free and (almost) guarantees you a first-place win if you use it. Use responsibly, and may God have mercy on your competitors' souls!

## Features:

- **Automated Refresh**: The bot refreshes the page and submits the token, allowing you to focus on the joy of watching your classmates try to win in vain.
- **Customizable**: Coordinates for submission can be customized based on your monitor's resolution. The script waits for a specified amount of time before performing the next action.
- **Coordinate Finder**: A separate utility (`coord-finder.py`) is available to help you find the mouse coordinates for the form submission.

## Requirements:

- Python 3.x
- `pyautogui` library. Here's the [documentation](https://pypi.org/project/PyAutoGUI/).
- `keyboard` library. Here's the [documentation](https://pypi.org/project/keyboard/).

## Installation:

1. Clone the repository to your local machine.
2. (Optional, but recommended) Create a virtual environment for the project.
3. Navigate to the project directory and run `pip install -r requirements.txt` to install the required packages.

## Configuration:

1. Use `coord-finder.py` to find the x and y coordinates for the area where the token gets posted. You can use the `mock_token_page.html` file to help you. Run the script and hover your mouse over the desired location. The coordinates will be displayed in the terminal.
2. Open `main.py` in your IDE of choice and update the `x_axis` and `y_axis` variables with the coordinates obtained from the previous step.
3. Modify the `sleep_seconds` variable to change the time delay between actions according to your browser's refresh rate. (I've found that 0.5 seconds works well for me, but it depends on your machine and internet speed.)

## Usage:

1. Open the token submission page.
2. Run `main.py` to execute the automation script.
3. Press `esc` to exit the script.

## Testing:

- I've included a _resources_ directory in this repo with a test page for your convenience. You can use this to test the script and ensure it works properly.
    - If the "I'm here!" button is clicked, several page items will change. Don't worry, it's pretty obvious when it works.
    - Be sure to refresh the page back to normal between tests.

## Troubleshooting:

- Sometimes you have to spam the `esc` key to exit the script. It's usually because the initial press happened during a process of the infinite loop. (I've never had to press it more than 3 times.)
- The functions in the script are pretty self-explanatory. If you're having trouble, try reading the code. By now all of us here should be pretty adept at reading and editing Python. I also have a few comments in there to help you out.
- Read the documentation for the `pyautogui` and `keyboard` libraries. Links are provided above.

## Disclaimer:

**Use this bot at your own risk!** I'm not liable if you break your machine (Not sure how that's even possible, to be honest. But haters gonna hate!) or get dismissed from the program while using this script. Ensure you follow all the guidelines and rules Hack Reactor and your cohort set. I will **not** offer any support for this script; you are all competent programmers and can figure it out.

## License:

MIT License

Copyright (c) 2023 Jeff Balagosa

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software") to deal
in the software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the software, and to permit persons to whom the software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the software.

THE SOFTWARE IS PROVIDED "AS IS"WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
