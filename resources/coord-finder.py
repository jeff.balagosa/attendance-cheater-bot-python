import pyautogui


def show_mouse_coordinates():
    print('Press Ctrl-C to quit.')
    try:
        while True:
            # get mouse position
            x, y = pyautogui.position()
            # format the position string and save it to a variable
            positionStr = f"X: {str(x).rjust(4)} Y: {str(y).rjust(4)}"
            # print the position string
            print(positionStr, end='')
            # erase the printed position string, to simulate positions being constantly updated
            print('\b' * len(positionStr), end='', flush=True)
    except KeyboardInterrupt:
        print('\n')


show_mouse_coordinates()
