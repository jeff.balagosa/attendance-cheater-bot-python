import pyautogui
import keyboard
import time

# IMPORTANT! PLEASE CONFIGURE AND TEST THIS SCRIPT BEFORE
# USING IT ON THE REAL PAGE!
# The mock page you can use to configure
# and test this script is located in the resources folder of this repo.
# Refer to the README and all comments in this file for more info.
x_axis = 1239
y_axis = 296
sleep_seconds = 1


def gui_actions_utility(x, y, sleep_time):
    # Refresh the page
    pyautogui.hotkey('ctrl', 'r')
    # Wait for the page to load
    time.sleep(sleep_time)
    # ddouble click token to select it
    pyautogui.doubleClick(x, y)
    # copy token
    pyautogui.hotkey('ctrl', 'c')
    # Tab to the input field
    pyautogui.hotkey('tab')
    # paste token into input field
    pyautogui.hotkey('ctrl', 'v')
    # tab to the "I'm here!" button
    pyautogui.hotkey('tab')
    # press enter to submit the form
    pyautogui.press('enter')
    return


def keyboard_listener():
    # Tally of how many times the script has run, helps with debugging
    tally = 0
    # Infinite loop to listen for key presses
    while True:
        # Activate the script by pressing F5
        if keyboard.is_pressed('F5'):
            # Infinite loop to run the function
            while True:
                gui_actions_utility(x_axis, y_axis, sleep_seconds)
                # increment the tally
                tally += 1
                print(f"Script ran {tally} times.")
                # Pause the script
                if keyboard.is_pressed('esc'):
                    print("Script paused.")
                    break
        # Exit the script
        if keyboard.is_pressed('esc'):
            print("Script exited.")
            break
    return


keyboard_listener()
